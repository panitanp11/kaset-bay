KasetBay::Application.routes.draw do
  if Rails.env.development?
    mount MailPreview => 'mail_preview'
  end

  get "log_in" => "sessions#new", :as => "log_in"
  get "log_out" => "sessions#destroy"
  get "sign_up" => "users#new", :as => "sign_up"
  get "activation_token/:token" => "users#activate", :as => "activation"
  get "recovery_token/:token" => "users#reset_password", :as => "reset_password"
  get "admin" => "admin#index", :as => "admin"
  get "tos" => "main#tos", :as => "tos"

  resources :sessions, :only => [:create]

  resources :users, :except => [:show] do
    collection do
      get :activate
      get :forget_password
      post :generate_recovery_token
    end

    member do
      get :resend_activation_email
    end
  end

  resources :sites, :except => [:new, :create] do
    member do
      get :dashboard
    end

    scope module: :sites do
      resource :publishing, :only => [:edit, :update]
    end

    resource :contact_info, :only => [:edit, :update]
    resources :menus, :except => [:show] do
      collection do
        post :sort
      end
      member do
        put :toggle_enabled
      end
    end

    resources :categories

    resources :pages, except: [:show] do
      collection do
        post :sort
      end

      resources :pictures, :only => [:index] do
        collection do
          put :create
        end
      end
    end
    resources :pictures, :only => [:destroy]
  end

  resources :categories, only: [], :path => "" do
    resources :pages, only: [:show], :path => ""
  end
  resources :pages, only: [:show], :path => ""

  constraints(Subdomain) do
    match "/" => "sites#show"
  end

  root :to => 'main#index'

end
