# Load the rails application
require File.expand_path('../application', __FILE__)

# Load settings and classes before environments/*.rb
if Rails.env.development?
  %w(development_settings mail_preview).each do |s|
    file = File.join(Rails.root, 'lib', 'kaset_bay', "#{s}.rb")
    load(file) if File.exists?(file)
  end
end

%w(settings subdomain decorators dummy_content).each do |s|
  file = File.join(Rails.root, 'lib', 'kaset_bay', "#{s}.rb")
  load(file) if File.exists?(file)
end

# Initialize the rails application
KasetBay::Application.initialize!
