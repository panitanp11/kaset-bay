# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140511153411) do

  create_table "categories", :force => true do |t|
    t.string   "title"
    t.string   "background_image_file_name"
    t.integer  "background_image_file_size"
    t.string   "background_image_content_type"
    t.datetime "background_image_updated_at"
    t.integer  "site_id"
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.boolean  "deleted",                       :default => false
  end

  add_index "categories", ["site_id"], :name => "index_categories_on_site_id"

  create_table "contact_infos", :force => true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "district"
    t.string   "city"
    t.string   "country"
    t.string   "phone"
    t.string   "fax"
    t.string   "email"
    t.integer  "site_id"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
    t.string   "facebook_url", :limit => 1000
    t.string   "zip_code"
  end

  add_index "contact_infos", ["site_id"], :name => "index_contact_infos_on_site_id"

  create_table "menus", :force => true do |t|
    t.string   "title"
    t.string   "hint"
    t.integer  "position"
    t.integer  "site_id"
    t.integer  "menuable_id"
    t.string   "menuable_type"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "enabled",       :default => true
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.text     "content"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.integer  "site_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "deleted",          :default => false
    t.integer  "category_id"
    t.integer  "position"
  end

  add_index "pages", ["category_id"], :name => "index_pages_on_category_id"
  add_index "pages", ["site_id"], :name => "index_pages_on_site_id"

  create_table "pictures", :force => true do |t|
    t.integer  "position"
    t.string   "picture_file_name"
    t.integer  "picture_file_size"
    t.string   "picture_content_type"
    t.datetime "picture_updated_at"
    t.integer  "picturable_id"
    t.string   "picturable_type"
    t.integer  "site_id"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "sites", :force => true do |t|
    t.string   "subdomain"
    t.string   "title"
    t.text     "sub_title"
    t.text     "meta_description"
    t.text     "meta_keywords"
    t.string   "logo_file_name"
    t.integer  "logo_file_size"
    t.string   "logo_content_type"
    t.datetime "logo_updated_at"
    t.string   "header_image_file_name"
    t.integer  "header_image_file_size"
    t.string   "header_image_content_type"
    t.datetime "header_image_updated_at"
    t.string   "background_image_file_name"
    t.integer  "background_image_file_size"
    t.string   "background_image_content_type"
    t.datetime "background_image_updated_at"
    t.boolean  "published",                     :default => false
    t.boolean  "deleted",                       :default => false
    t.integer  "user_id"
    t.datetime "created_at",                                           :null => false
    t.datetime "updated_at",                                           :null => false
    t.string   "sidebar_color",                 :default => "#383D40"
    t.string   "article_color",                 :default => "#F5F2F1"
    t.string   "footer_color",                  :default => "#7CBDA1"
  end

  add_index "sites", ["deleted"], :name => "index_sites_on_deleted"
  add_index "sites", ["published"], :name => "index_sites_on_published"
  add_index "sites", ["subdomain"], :name => "index_sites_on_subdomain", :unique => true
  add_index "sites", ["user_id"], :name => "index_sites_on_user_id"

  create_table "users", :force => true do |t|
    t.string   "email"
    t.string   "activation_token"
    t.string   "recovery_token"
    t.string   "sign_up_ip"
    t.boolean  "active",           :default => true
    t.boolean  "deleted",          :default => false
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.string   "password_digest"
  end

  add_index "users", ["activation_token"], :name => "index_users_on_activation_token"
  add_index "users", ["active"], :name => "index_users_on_active"
  add_index "users", ["deleted"], :name => "index_users_on_deleted"
  add_index "users", ["email"], :name => "index_users_on_email"

end
