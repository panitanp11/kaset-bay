class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :title
      t.string :hint
      t.integer :position

      t.references :site
      t.references :menuable, polymorphic: true

      t.timestamps
    end
  end
end
