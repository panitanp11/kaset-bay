class CreateSites < ActiveRecord::Migration
  def change
    create_table :sites do |t|
      t.string :subdomain
      t.string :title
      t.text :sub_title
      t.text :meta_description
      t.text :meta_keyword

      t.string :logo_file_name
      t.integer :logo_file_size
      t.string :logo_content_type
      t.datetime :logo_updated_at

      t.string :header_image_file_name
      t.integer :header_image_file_size
      t.string :header_image_content_type
      t.datetime :header_image_updated_at

      t.string :background_image_file_name
      t.integer :background_image_file_size
      t.string :background_image_content_type
      t.datetime :background_image_updated_at

      t.boolean :published, default: false
      t.boolean :deleted, default: false

      t.references :user

      t.timestamps
    end

    add_index :sites, :subdomain, unique: true
    add_index :sites, :published
    add_index :sites, :deleted
    add_index :sites, :user_id
  end
end
