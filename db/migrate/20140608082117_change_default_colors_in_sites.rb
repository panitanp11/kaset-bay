class ChangeDefaultColorsInSites < ActiveRecord::Migration
  def up
    change_column_default :sites, :sidebar_color, "#445878"
    change_column_default :sites, :article_color, "#EEEFF7"
    change_column_default :sites, :footer_color, "#92CDCF"
  end

  def down
    change_column_default :sites, :sidebar_color, "#383D40"
    change_column_default :sites, :article_color, "#F5F2F1"
    change_column_default :sites, :footer_color, "#7CBDA1"
  end
end
