class CreateContactInfos < ActiveRecord::Migration
  def change
    create_table :contact_infos do |t|
      t.string :name
      t.text :address
      t.string :district
      t.string :city
      t.string :country
      t.string :phone
      t.string :fax
      t.string :email

      t.references :site

      t.timestamps
    end

    add_index :contact_infos, :site_id
  end
end
