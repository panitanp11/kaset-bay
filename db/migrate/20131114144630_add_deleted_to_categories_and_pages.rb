class AddDeletedToCategoriesAndPages < ActiveRecord::Migration
  def up
    add_column :categories, :deleted, :boolean, default: false
    add_column :pages, :deleted, :boolean, default: false
  end

  def down
    remove_column :categories, :deleted
    remove_column :pages, :deleted
  end
end
