class AddFacebookUrlToContactInfos < ActiveRecord::Migration
  def change
    add_column :contact_infos, :facebook_url, :string, limit: 1000
  end
end
