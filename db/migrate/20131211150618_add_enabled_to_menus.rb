class AddEnabledToMenus < ActiveRecord::Migration
  def change
    add_column :menus, :enabled, :boolean, default: true
  end
end
