class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.integer :position

      t.string :picture_file_name
      t.integer :picture_file_size
      t.string :picture_content_type
      t.datetime :picture_updated_at

      t.references :picturable, polymorphic: true
      t.references :site

      t.timestamps
    end
  end
end
