class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.string :title
      t.text :content
      t.text :meta_description
      t.text :meta_keywords

      t.references :site

      t.timestamps
    end

    add_index :pages, :site_id
  end
end
