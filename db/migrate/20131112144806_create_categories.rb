class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title

      t.string :background_image_file_name
      t.integer :background_image_file_size
      t.string :background_image_content_type
      t.datetime :background_image_updated_at

      t.references :site

      t.timestamps
    end

    add_index :categories, :site_id
  end
end
