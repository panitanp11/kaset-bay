class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_hash
      t.string :password_salt
      t.string :activation_token
      t.string :recovery_token
      t.string :sign_up_ip
      t.boolean :active, default: true
      t.boolean :deleted, default: false

      t.timestamps
    end

    add_index :users, :email
    add_index :users, :activation_token
    add_index :users, :active
    add_index :users, :deleted
  end
end
