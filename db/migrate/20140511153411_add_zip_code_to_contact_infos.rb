class AddZipCodeToContactInfos < ActiveRecord::Migration
  def change
    add_column :contact_infos, :zip_code, :string
  end
end
