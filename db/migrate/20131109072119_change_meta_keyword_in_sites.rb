class ChangeMetaKeywordInSites < ActiveRecord::Migration
  def up
    rename_column :sites, :meta_keyword, :meta_keywords
  end

  def down
    rename_column :sites, :meta_keywords, :meta_keyword
  end
end
