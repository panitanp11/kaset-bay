class AddColorsToSites < ActiveRecord::Migration
  def change
    add_column :sites, :sidebar_color, :string, default: "#383D40"
    add_column :sites, :article_color, :string, default: "#F5F2F1"
    add_column :sites, :footer_color, :string, default: "#7CBDA1"
  end
end
