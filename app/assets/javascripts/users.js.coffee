$(document).ready ->
  $("#toggle_password").click ->
    password = $("#user_password")
    if password.prop("type") == "password"
      password.prop "type", "text"
      $(this).find("div").removeClass("glyphicon-eye-close")
      $(this).find("div").addClass("glyphicon-eye-open")
    else
      password.prop "type", "password"
      $(this).find("div").removeClass("glyphicon-eye-open")
      $(this).find("div").addClass("glyphicon-eye-close")
