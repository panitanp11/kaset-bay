// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap.min
//= require jquery.ui.widget.js
//= require jquery.iframe-transport.js
//= require jquery.fileupload.js
//= require jquery.ui.sortable.js
//= require fancybox
//= require bootstrap-wysihtml5
//= require blur.js
//= require spectrum.js
//= require jquery.easing.min.js
//= require modernizr.js

//= require blurry
//= require color_pickers
//= require file_upload
//= require hider
//= require menus
//= require picture_fancybox
//= require sortable
//= require users
//= require wysihtml5
