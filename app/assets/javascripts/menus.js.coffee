$(document).ready ->
  if $('[name="menu[menuable_type]"]').length > 0
    category = $("#menu_menuable_id_category").detach()
    page = $("#menu_menuable_id_page").detach()

    menuable_id_toggle = ->
      if $('input[id="menu_menuable_type_category"]').is(":checked")
        $("#menu_menuable_id").html(category)
      if $('input[id="menu_menuable_type_page"]').is(":checked")
        $("#menu_menuable_id").html(page)

    $('[name="menu[menuable_type]"]').change ->
      menuable_id_toggle()

    menuable_id_toggle()
