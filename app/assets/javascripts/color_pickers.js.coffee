$(document).ready ->
  $("[data-color-picker]").each ->
    $(this).spectrum
      color: $(this).val()
      showButtons: false
      change: (color) ->
        $(this).attr('value', color)
