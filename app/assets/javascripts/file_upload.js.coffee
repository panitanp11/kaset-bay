$(document).ready ->
  $("#fileupload").fileupload
    dataType: "json"
    start: ->
      $("#file_field_form").toggle()
      $("#file_field_loading").toggle()
    fail: (e, data) ->
      gallery = $("#gallery")
      err_msg = gallery.data("err-msg")
      alert(err_msg) if err_msg
    always: ->
      gallery = $("#gallery")
      path = gallery.data("gallery-path")
      if path
        $.ajax(url: path).done (html) ->
          $("#gallery").html(html)
      $("#file_field_form").toggle()
      $("#file_field_loading").toggle()
