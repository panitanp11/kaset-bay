$(document).ready ->
  $("#sortable").sortable
    axis: "y"
    containment: "parent"
    cursor: "move"
    dropOnEmpty: false
    handle: "[data-sortable-handle]"
    opacity: 0.75
    scroll: true
    tolerance: "pointer"
    update: ->
      $.ajax
        type: "post",
        data: $("#sortable").sortable("serialize", { key: "position[]" }),
        dataType: 'script',
        url: $("#sortable").data("url")
