$(document).ready ->

  $(".wysihtml5").each (i, elem) ->
    $(elem).wysihtml5
      events:
        load: ->
          $(".wysihtml5-toolbar a.btn").addClass('btn-default')
      lists: false
      link: false
      image: false
      color: true
