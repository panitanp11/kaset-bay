$(document).ready ->
  $('.blurry').blurjs
    source: '#content'
    overlay: 'rgba(100,100,100,0.4)'
    radius: 5
    cache: true
