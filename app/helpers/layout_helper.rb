module LayoutHelper
  def page_title(title)
    @page_title = title
  end

  def flash_bar_tag(name, msg)
    content_tag :div, msg, class: "flash flash_#{name}"
  end

  def active_page(path)
    "active" if current_page?(path)
  end

  def max_width_image_tag(image_url, width, options = {})
    image_tag image_url, options.merge!(style: "max-width: #{width}px;")
  end

  def edit_link(path)
    link_to path do
      content_tag(:span, nil, class: "glyphicon glyphicon-edit mr") + t('shared.edit')
    end
  end

  def enable_disable_link(path, enabled)
    link_to path, method: "put" do
      if enabled
        content_tag(:span, nil, class: "glyphicon glyphicon-pause mr") + t('shared.disable')
      else
        content_tag(:span, nil, class: "glyphicon glyphicon-play mr") + t('shared.enable')
      end
    end
  end

  def delete_link(path)
    link_to path, confirm: t('shared.confirm_delete'), method: "delete" do
      content_tag(:span, nil, class: "glyphicon glyphicon-trash mr") + t('shared.delete')
    end
  end

  def publishing_checklist(status, path = nil)
    sign = status ? "ok-sign" : "remove-sign"
    content_tag :div do
      concat content_tag(:span, nil, class: "glyphicon glyphicon-#{sign} #{sign} vl")
      unless status
        concat "<br>".html_safe
        concat link_to(t('sites.publishings.edit.fix'), path)
      end
    end
  end

  def website_unpublished(published)
    content_tag :div, class: "p #{'selected' unless published}" do
      concat content_tag(:span, nil, class: "glyphicon glyphicon-pause pause")
      concat content_tag(:div, t('sites.publishings.edit.unpublished'))
    end
  end

  def website_published(published)
    content_tag :div, class: "p #{'selected' if published}" do
      concat content_tag(:span, nil, class: "glyphicon glyphicon-play play")
      concat content_tag(:div, t('sites.publishings.edit.published'))
    end
  end

end
