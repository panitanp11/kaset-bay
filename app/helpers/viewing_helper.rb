module ViewingHelper
  def background_image(site, category)
    if category
      category.background_image
    else
      site.background_image
    end
  end

  def meta_contents(object)
    @meta_description = object.meta_description if object.respond_to?(:meta_description)
    @meta_keywords = object.meta_keywords if object.respond_to?(:meta_keywords)
  end

  def menu_link_title(menu)
    if menu.hint.present?
      menu.hint
    else
      "#{menu.title} | #{menu.site.title}"
    end
  end

end
