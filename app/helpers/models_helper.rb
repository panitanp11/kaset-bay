module ModelsHelper
  def orphan_page?(page)
    page.orphan?
  end

  def page_belong_to_category?(page, category)
    if category.persisted?
      category.pages.include? page
    end
  end

  def pages_inside(category)
    if category.pages.any?
      category.pages.map do |page|
        link_to page.title, edit_site_page_path(@site, page)
      end.join(", ").html_safe
    end
  end

end
