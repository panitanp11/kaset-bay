class SystemMailer < ActionMailer::Base
  layout "system_mailer"

  default from: ADMIN_EMAIL

  def new_sign_up(user)
    if user
      @email = user.email
      @activation_token = user.activation_token
      @website = user.site.url if user.site

      mail to: @email,
           subject: t('system_mailer.new_sign_up.subject')
    end
  end

  def reset_password(user)
    if user && (@recovery_token = user.recovery_token)
      @email = user.email

      mail to: @email,
           subject: t('system_mailer.reset_password.subject', domain_host: DOMAIN_HOST)
    end
  end

end
