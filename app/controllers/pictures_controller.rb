class PicturesController < ApplicationController
  before_filter :get_site
  before_filter :owner_or_admin_required
  before_filter :get_page, only: [:index, :create]
  before_filter :picture_limit, only: [:create]

  def index
    render partial: "pictures/listing", locals: { pictures: @page.pictures }
  end

  def create
    if @site && @page && (picture = params[:uploaded]).present?
      picture = Picture.new(picture: picture)
      picture.picturable = @page
      picture.site = @site
      success = picture.save
    end

    if success
      render json: { success: success }.to_json
    else
      render status: 403
    end
  end

  def destroy
    picture = Picture.find_by_id(params[:id])
    if picture
      picture.destroy
      flash.notice = t('pictures.destroy.success')
      redirect_to edit_site_page_path(@site, picture.picturable)
    else
      render nothing: true
    end

  end

  private

  def get_page
    @page = Page.find_by_id params[:page_id]
  end

  def picture_limit
    if !admin? && @site.pictures.count >= PICTURE_LIMIT
      render status: 403
    end
  end

end
