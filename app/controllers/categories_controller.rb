class CategoriesController < ApplicationController
  before_filter :get_site
  before_filter :owner_or_admin_required
  before_filter :get_category, except: [:index]
  before_filter :get_all_pages, except: [:index, :destroy]
  before_filter :category_limit, only: [:new, :create]

  def index
    @categories = @site.categories.active.ordered
  end

  def new
  end

  def create
    @category.site = @site
    if @category.update_attributes params[:category]
      @category.reassign_pages *params[:page_ids]

      flash.notice= t('categories.create.success')
      redirect_to action: "index"
    else
      flash.now.alert = t('categories.create.failure')
      render "new"
    end
  end

  def edit
  end

  def update
    if @category.update_attributes params[:category]
      @category.reassign_pages *params[:page_ids]

      flash.notice= t('categories.update.success')
      redirect_to action: "index"
    else
      flash.now.alert = t('categories.update.failure')
      render "edit"
    end
  end

  def destroy
    @category.destroy
    flash.notice = t('categories.destroy.success', name: @category.title)

    redirect_to action: "index"
  end

  private

  def get_category
    case action_name
    when "new", "create"
      @category = Category.new
    else
      @category = Category.find_by_id params[:id]
    end
  end

  def get_all_pages
    @all_pages = @site.pages.ordered
  end

  def category_limit
    if !admin? && @site.categories.count >= CATEGORY_LIMIT
      flash.alert = t('categories.shared.limitation')
      redirect_to site_categories_path(@site)
    end
  end

end
