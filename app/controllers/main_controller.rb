class MainController < ApplicationController
  skip_before_filter :authentication_required

  layout "cover_page"

  def index
    if current_user && current_user.sites.any?
      redirect_to dashboard_site_path(current_user.site), flash: flash
    end
  end

  def tos
  end

end
