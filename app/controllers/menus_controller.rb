class MenusController < ApplicationController
  before_filter :get_site
  before_filter :owner_or_admin_required
  before_filter :get_menu, except: [:index, :sort]
  before_filter :get_categories_and_pages, only: [:new, :create, :edit, :update]

  def index
    @menus = @site.menus.ordered
  end

  def new
  end

  def create
    @menu.site = @site
    if @menu.update_attributes params[:menu]
      @menu.update_column :position, @site.menus.count
      flash.notice= t('menus.create.success')
      redirect_to action: "index"
    else
      flash.now.alert = t('menus.create.failure')
      render "new"
    end
  end

  def edit
  end

  def update
    if @menu.update_attributes params[:menu]
      flash.notice= t('menus.update.success')
      redirect_to action: "index"
    else
      flash.now.alert = t('menus.update.failure')
      render "edit"
    end
  end

  def destroy
    @menu.destroy
    flash.notice = t('menus.destroy.success', name: @menu.title)

    redirect_to action: "index"
  end

  def sort
    if request.xhr? && params[:position]
      params[:position].each_with_index do |menu_id, index|
        if menu = Menu.find_by_id(menu_id)
          menu.update_column :position, index
        end
      end
    end
    render nothing: true
  end

  def toggle_enabled
    @menu.toggle! :enabled

    redirect_to action: "index"
  end

  private

  def get_menu
    case action_name
    when "new", "create"
      @menu = Menu.new
    else
      @menu = Menu.find_by_id params[:id]
    end
  end

  def get_categories_and_pages
    @all_categories = @site.categories.not_empty.ordered
    @all_pages = @site.pages.ordered
  end

end
