class PagesController < ApplicationController
  before_filter :get_site
  before_filter :owner_or_admin_required, except: [:show]
  before_filter :get_page, except: [:index, :sort]
  before_filter :get_pictures, only: [:edit, :update, :show]
  before_filter :get_category, only: [:show]
  before_filter :page_limit, only: [:new, :create]
  before_filter :published_or_authenticated?, only: [:show]

  layout :resolve_layout

  def index
    @pages = @site.pages.active.ordered
  end

  def show
    @menus = @site.menus.enabled.ordered
  end

  def new
  end

  def create
    @page.site = @site
    if @page.update_attributes params[:page]
      @page.update_column :position, @site.pages.count
      flash.notice= t('pages.create.success')
      redirect_to edit_site_page_path(@site, @page)
    else
      flash.now.alert = t('pages.create.failure')
      render "new"
    end
  end

  def edit
    if @site.pictures.count >= PICTURE_LIMIT
      flash.now.notice= t('pictures.create.limit')
    end
  end

  def update
    if @page.update_attributes params[:page]
      flash.notice= t('pages.update.success')
      redirect_to action: "index"
    else
      flash.now.alert = t('pages.update.failure')
      render "edit"
    end
  end

  def destroy
    @page.destroy
    flash.notice = t('pages.destroy.success', name: @page.title)

    redirect_to action: "index"
  end

  def sort
    if request.xhr? && params[:position]
      params[:position].each_with_index do |page_id, index|
        if page = Page.find_by_id(page_id)
          page.update_column :position, index
        end
      end
    end
    render nothing: true
  end

  private

  def get_page
    case action_name
    when "new", "create"
      @page = Page.new
    when "show"
      @page = @site.pages.find params[:id]
    else
      @page = Page.find params[:id]
    end
  end

  def get_pictures
    @pictures = @page.pictures
  end

  def get_category
    if params[:category_id]
      @category = Category.find params[:category_id]
    end
  end

  def page_limit
    if !admin? && @site.pages.count >= PAGE_LIMIT
      flash.alert = t('pages.shared.limitation')
      redirect_to site_pages_path(@site)
    end
  end

end
