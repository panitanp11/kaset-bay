class ContactInfosController < ApplicationController
  before_filter :get_site
  before_filter :owner_or_admin_required
  before_filter :get_contact_info

  def edit
  end

  def update
    @contact_info.site = @site
    if @contact_info.update_attributes params[:contact_info]
      flash.now.notice= t('contact_infos.update.success')
    else
      flash.now.alert = t('contact_infos.update.failure')
    end

    render "edit"
  end

  private

  def get_contact_info
    @contact_info = @site.contact_info||ContactInfo.new
  end

end
