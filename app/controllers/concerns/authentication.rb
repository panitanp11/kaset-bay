module Authentication
  extend ActiveSupport::Concern

  included do
    helper_method :current_user
    helper_method :authenticated?
  end

  def log_in!(user)
    session[:user_id] = user.id if user
  end

  def log_out!
    session[:user_id] = nil
  end

  def authenticated?
    !!current_user
  end

  def current_user
    @current_user = User.find(session[:user_id]) if session[:user_id]
  end
end
