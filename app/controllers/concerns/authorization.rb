module Authorization
  extend ActiveSupport::Concern

  included do
    helper_method :admin?
  end

  def authentication_required
    if !authenticated?
      redirect_to log_in_path, flash: flash
    end
  end

  def owner_or_admin_required
    if !(owner? || admin?)
      render_forbidden
    end
  end

  def admin_required
    render_forbidden unless admin?
  end

  private

  def owner?
    if @site.present? && current_user
      @site.owner.id == current_user.id
    else
      true
    end
  end

  def admin?
    current_user.id == 1
  end

end
