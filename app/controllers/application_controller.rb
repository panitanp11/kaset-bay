class ApplicationController < ActionController::Base
  protect_from_forgery

  helper LayoutHelper
  helper ViewingHelper
  include Authentication
  include Authorization

  before_filter :authentication_required, except: [:show]

  private

  def get_site
    case action_name
    when "show"
      @site = Site.find_by_subdomain! request.subdomain
    else
      @site = Site.find_by_id params[:site_id]
    end
  end

  def published_or_authenticated?
    unless authenticated? || @site.published?
      render_page_not_found
    end
  end

  def resolve_layout
    case action_name
    when "show"
      "viewing"
    else
      "application"
    end
  end

  def redirect_to_dashboard_if_authenticated
    if authenticated?
      redirect_to root_path
    end
  end

  def render_page_not_found
    raise ActionController::RoutingError.new('Page Not Found')
  end

  def render_forbidden
    render file: Rails.root.join("public/403.html"), status: 403, layout: false
  end

end
