module Sites
  class PublishingsController < ApplicationController
    before_filter :get_site
    before_filter :owner_or_admin_required

    def edit
    end

    def update
      if (@site.published? || @site.can_published?) && @site.toggle_published
        flash.notice = t('sites.publishings.update.success')
      else
        flash.alert = t('sites.publishings.update.failure')
      end

      redirect_to edit_site_publishing_path(@site)
    end

  end
end
