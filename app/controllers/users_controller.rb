class UsersController < ApplicationController
  before_filter :get_user, only: [:new, :create, :edit, :update, :destroy]
  before_filter :get_site, only: [:edit, :update]
  before_filter :owner_or_admin_required, only: [:edit, :update]
  before_filter :redirect_to_dashboard_if_authenticated, only: [:new, :create]
  skip_before_filter :authentication_required, except: [:index, :edit, :update, :destroy]

  def index
  end

  def new
    @site = Site.new
  end

  def create
    @site = Site.new params[:site]

    @user.sign_up_ip = request.env["REMOTE_ADDR"]
    @user.activation_token = SecureRandom.hex
    @site.user = @user

    valid_user, valid_site = @user.valid?, @site.valid?

    if valid_user && valid_site && @user.save && @site.save
      DummyContent.new(@site).build_dummy_content
      log_in! @user
      SystemMailer.new_sign_up(@user).deliver
      redirect_to root_url, notice: t('users.create.success')
    else
      flash.now.alert = t('users.create.failure')
      render "new"
    end
  end

  def edit
  end

  def update
    if @user.update_attributes params[:user]
      flash.now.notice= t('users.update.success')
    else
      flash.now.alert = t('users.update.failure')
    end

    render "edit"
  end

  def destroy
    @user.update_column :deleted, true
    raise "destroy"
  end

  def activate
    if @user = User.find_by_activation_token(params[:token])
      @user.update_column :activation_token, nil
      log_in! @user
      redirect_to root_url, notice: t('users.activate.success')
    else
      redirect_to root_url, alert: t('users.activate.failure')
    end
  end

  def resend_activation_email
    SystemMailer.new_sign_up(current_user).deliver
    flash.notice = t('users.resend_activation_email')
    redirect_to request.referrer.gsub(/[?].*/, "")
  end

  def forget_password
  end

  def generate_recovery_token
    if params[:email] && user = User.find_by_email(params[:email].strip)
      user.update_column :recovery_token, SecureRandom.hex

      SystemMailer.reset_password(user).deliver
      flash.now.notice = t('users.generate_recovery_token.success')
    else
      flash.now.alert = t('users.generate_recovery_token.failure')
    end

    render "forget_password"
  end

  def reset_password
    if user = User.find_by_recovery_token(params[:token])
      user.update_column :recovery_token, nil
      log_in! user
      redirect_to edit_user_path(user), notice: t('users.reset_password.success')
    else
      redirect_to root_url, alert: t('users.reset_password.failure')
    end
  end

  private

  def get_site
    @site = @user.site
  end

  def get_user
    case action_name
    when "new"
      @user = User.new
    when "create"
      @user = User.new params[:user]
    when "edit", "update", "destroy"
      @user = User.find_by_id params[:id]
    end
  end

end
