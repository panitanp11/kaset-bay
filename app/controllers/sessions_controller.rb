class SessionsController < ApplicationController
  before_filter :redirect_to_dashboard_if_authenticated, except: [:destroy]
  skip_before_filter :authentication_required

  def new
  end

  def create
    if user = User.authenticate(params[:email], params[:password])
      log_in! user
      redirect_to root_path, notice: t('sessions.create.success')
    else
      flash.now.alert = t('sessions.create.failure')
      render "new"
    end
  end

  def destroy
    log_out!
    redirect_to log_in_path, notice: t('sessions.destroy.success')
  end

end
