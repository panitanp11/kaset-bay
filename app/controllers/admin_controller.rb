class AdminController < ApplicationController
  before_filter :admin_required

  def index
    @sites = Site.ordered
    @users = User.ordered
  end

end
