class SitesController < ApplicationController
  before_filter :get_site, except: [:index]
  before_filter :owner_or_admin_required, except: [:index]
  before_filter :published_or_authenticated?, only: [:show]

  layout :resolve_layout

  def index
  end

  def show
    @site = Site.find_by_subdomain! request.subdomain
    @menus = @site.menus.enabled.ordered

    if @menus.any?
      @page = @menus.first.is_category? ? @menus.first.menuable.pages.first : @menus.first.menuable
      @pictures = @page.pictures
      render template: "pages/show"
    end
  end

  def edit
  end

  def update
    if @site.update_attributes params[:site]
      flash.now.notice= t('sites.update.success')
    else
      flash.now.alert = t('sites.update.failure')
    end

    render "edit"
  end

  def destroy
    @site.update_column :deleted, true
    raise "destroy"
  end

  def dashboard
  end

  private

  def get_site
    case action_name
    when "show"
      @site = Site.find_by_subdomain! request.subdomain
    else
      @site = Site.find_by_id params[:id]
    end
  end

end
