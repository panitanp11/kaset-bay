class Category < ActiveRecord::Base
  attr_accessible :title, :background_image

  has_many :pages
  has_many :menus, as: :menuable
  belongs_to :site

  BACKGROUND_IMAGE_STYLES = { normal: "1280x800#" }
  has_attached_file :background_image, styles: BACKGROUND_IMAGE_STYLES, default_style: :normal,
                    default_url: "/assets/default/background.jpg"

  validates :title, presence: true, length: { maximum: 255 }
  validates_attachment :background_image,
                       content_type: { content_type: ACCEPTED_IMAGE_TYPES, message: I18n.translate('shared.error_messages.file_type') },
                       size: { less_than: 10.megabytes, message: I18n.translate('shared.error_messages.file_size') }

  default_scope where(deleted: false)
  scope :active, where(deleted: false)
  scope :not_empty, joins(:pages).where("pages.id IS NOT NULL AND pages.deleted = 'f'").uniq
  scope :ordered, order("title asc")

  def to_param
    "#{id}-#{slug}"
  end

  def slug
    title.downcase.gsub(" ", "-").gsub(".", "").gsub("/", "").gsub("%", "")
  end

  def reassign_pages(*page_ids)
    self.pages.delete_all

    page_ids.each do |page_id|
      if page = Page.find_by_id(page_id)
        self.pages << page
      end
    end

    clear_associations if pages.empty?
  end

  def destroy
    self.update_column :deleted, true
    clear_associations
  end

  private

  def clear_associations
    self.menus.each do |menu|
      menu.update_column :menuable_id, nil
      menu.update_column :menuable_type, nil
    end
  end

end
