class Menu < ActiveRecord::Base
  attr_accessible :title, :hint, :menuable_type, :menuable_id

  belongs_to :menuable, polymorphic: true
  belongs_to :site

  validates :title, presence: true, length: { maximum: 255 }
  validates :menuable, presence: true

  scope :ordered, -> { order(:position) }
  scope :enabled, -> { where(enabled: true) }

  after_commit :no_of_enabled_menus

  def is_category?
    menuable_type == "Category"
  end

  def is_page?
    menuable_type == "Page"
  end

  private

  def no_of_enabled_menus
    site.toggle_published if !site.at_least_one_enabled_menu? && site.published
  end

end
