class User < ActiveRecord::Base
  has_secure_password

  attr_accessible :email, :password

  has_many :sites, dependent: :destroy

  before_validation :prepare_user

  validates :email, presence: true, format: { with: /[\w+\-.]+@[a-z\d\-.]+\.[a-z]+/i },
                    uniqueness: true
  validates :password, length: { within: 6..40 }

  scope :active, -> { where(active: true) }
  scope :ordered, -> { order(:id) }

  def to_s
    email
  end

  def site
    sites.first if sites.any?
  end

  def activated?
    activation_token.nil?
  end

  def self.authenticate(email, password)
    sleep 1
    if (user = find_by_email(email)) && user.active && user.authenticate(password)
      user
    end
  end

  private

  def prepare_user
    self.email = self.email.downcase
  end

end
