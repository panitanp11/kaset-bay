class Page < ActiveRecord::Base
  attr_accessible :title, :content, :meta_description, :meta_keywords

  has_many :pictures, as: :picturable, dependent: :destroy
  has_many :menus, as: :menuable
  belongs_to :site
  belongs_to :category

  validates :title, presence: true, length: { maximum: 255 }
  validates :content, presence: true

  default_scope where(deleted: false)
  scope :active, where(deleted: false)
  scope :ordered, order(:position)

  def to_param
    "#{id}-#{slug}"
  end

  def slug
    title.downcase.gsub(" ", "-").gsub(".", "").gsub("/", "").gsub("%", "")
  end

  def orphan?
    category.nil?
  end

  def destroy
    self.category = nil
    self.deleted = true
    self.save
    clear_associations

    pictures.each { |picture| picture.destroy }
  end

  private

  def clear_associations
    menus.each do |menu|
      menu.update_column :menuable_id, nil
      menu.update_column :menuable_type, nil
    end
  end

end
