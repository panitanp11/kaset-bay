class ContactInfo < ActiveRecord::Base
  attr_accessible :name, :address, :district, :city, :country,
                  :phone, :fax, :email, :facebook_url, :zip_code

  belongs_to :site

  before_validation :prepare_contact_info

  validates :name, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :country, presence: true
  validates :phone, presence: true, format: { with: /^(\d|-|\+|\s)+$/ }
  validates :fax, allow_blank: true, format: { with: /^(\d|-|\+|\s)+$/ }
  validates :email, allow_blank: true, format: { with: /[\w+\-.]+@[a-z\d\-.]+\.[a-z]+/i }
  validates :facebook_url, allow_blank: true, length: { maximum: 999 }

  private

  def prepare_contact_info
    self.country = DEFAULT_COUNTRY
  end

end
