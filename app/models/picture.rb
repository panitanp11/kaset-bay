class Picture < ActiveRecord::Base
  attr_accessible :position, :picture

  belongs_to :site
  belongs_to :picturable, polymorphic: true

  PICTURE_STYLES = { normal: "x800", thumb: "x125" }
  has_attached_file :picture, styles: PICTURE_STYLES, default_style: :normal,
                    default_url: "/logo/default.png"

  validates_attachment :picture,
                       content_type: { content_type: ACCEPTED_IMAGE_TYPES, message: I18n.translate('shared.error_messages.file_type') },
                       size: { less_than: 10.megabytes, message: I18n.translate('shared.error_messages.file_size') }

  scope :ordered, order("position asc")

end
