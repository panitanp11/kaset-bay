class Site < ActiveRecord::Base
  attr_accessible :subdomain, :title, :sub_title, :meta_description, :meta_keywords,
                  :sidebar_color, :article_color, :footer_color,
                  :logo, :header_image, :background_image

  has_one :contact_info, dependent: :destroy
  has_many :menus, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :pages, dependent: :destroy
  has_many :pictures, dependent: :destroy
  belongs_to :user

  LOGO_STYLES = { normal: "x200" }
  has_attached_file :logo, styles: LOGO_STYLES, default_style: :normal,
                    default_url: "/assets/default/logo.png"

  HEADER_IMAGE_STYLES = { normal: "960x250#" }
  has_attached_file :header_image, styles: HEADER_IMAGE_STYLES, default_style: :normal,
                    default_url: "/assets/default/header.jpg"

  BACKGROUND_IMAGE_STYLES = { normal: "1280x800#" }
  has_attached_file :background_image, styles: BACKGROUND_IMAGE_STYLES, default_style: :normal,
                    default_url: "/assets/default/background.jpg"

  before_validation :prepare_site

  validates :subdomain, presence: true, format: { with: /^(?!^www$)[a-z0-9]+$/i },
                        length: { minimum: 3 }, uniqueness: true
  validates :sidebar_color, presence: true, format: { with: /#(\d|[abcdefABCDEF]){6}/ }
  validates :article_color, presence: true, format: { with: /#(\d|[abcdefABCDEF]){6}/ }
  validates :footer_color, presence: true, format: { with: /#(\d|[abcdefABCDEF]){6}/ }
  validates_attachment :logo,
                       content_type: { content_type: ACCEPTED_IMAGE_TYPES, message: I18n.translate('shared.error_messages.file_type') },
                       size: { less_than: 10.megabytes, message: I18n.translate('shared.error_messages.file_size') }
  validates_attachment :header_image,
                       content_type: { content_type: ACCEPTED_IMAGE_TYPES, message: I18n.translate('shared.error_messages.file_type') },
                       size: { less_than: 10.megabytes, message: I18n.translate('shared.error_messages.file_size') }
  validates_attachment :background_image,
                       content_type: { content_type: ACCEPTED_IMAGE_TYPES },
                       size: { less_than: 10.megabytes, message: I18n.translate('shared.error_messages.file_size') }

  scope :ordered, -> { order(:id) }

  def owner
    user
  end

  def url
    "http://" + subdomain + "." + DOMAIN_HOST
  end

  def toggle_published
    self.toggle! :published
  end

  def at_least_one_enabled_menu?
    menus.enabled.any?
  end

  def can_published?
    user.activated? &&
    contact_info.present? &&
    at_least_one_enabled_menu?
  end

  private

  def prepare_site
    self.subdomain = self.subdomain.downcase
  end

end
