class DummyContent
  DUMMY_CONTACT_INFO = { name: "นาไร่สวนไทยไทย", address: "123 ถนนไทยทำ ตำบลไทยใช้ (แก้ไขข้อมูลไปที่ การตั้งค่า -> ตั้งค่าการติดต่อ)", city: "ไทยเจริญ", phone: "123-456-789" }
  DUMMY_MENUS = [
                  { menu: { attr: { title: "หน้าแรก" },
                            type: "page",
                            page: [ { attr: { title: "นาไร่สวนไทยไทย", content: "เราทำการเกษตรแบบพอเพียง.........<br>(แก้ไขข้อมูลและเพิ่มรูป ไปที่ เนื้อหา -> หน้า -> นาไร่สวนไทยไทย)<br><br>เวบไซต์ขณะนี้ประกอบด้วย 4 เมนูคือ<br>- นาไร่สวนไทยไทย<br>- เมนูที่ 2: การเดินทางและแผนที่<br>- เมนูที่ 3: การเกษตรแบบพอเพียง<br>- เมนูที่ 4: ผลิตภัณฑ์การเกษตร<br><br>ต้องการแก้ไข/เพิ่ม/ลดเมนู ไปที่ เนื้อหา -> เมนู" } } ] } },
                  { menu: { attr: { title: "เมนูที่ 2: การเดินทางและแผนที่" },
                            type: "page",
                            page: [ { attr: { title: "ที่ตั้งนาไร่สวนไทยไทย", content: "นาไร่สวนของเราตั้งอยู่ที่.........<br>(แก้ไขข้อมูลและเพิ่มรูป ไปที่ เนื้อหา -> หน้า -> ที่ตั้งนาไร่สวนไทยไทย)" } } ] } },
                  { menu: { attr: { title: "เมนูที่ 3: การเกษตรแบบพอเพียง" },
                            type: "page",
                            page: [ { attr: { title: "ทำไมต้องทำการเกษตรแบบพอเพียง", content: "เพราะว่า.........<br>(แก้ไขข้อมูลและเพิ่มรูป ไปที่ เนื้อหา -> หน้า -> ทำไมต้องทำการเกษตรแบบพอเพียง)" } } ] } },
                  { menu: { attr: { title: "เมนูที่ 4: ผลิตภัณฑ์การเกษตร" },
                            type: "category",
                            category: { attr: { title: "ผลิตภัณฑ์" } },
                            pages: [ { attr: { title: "มะยงชิด", content: "มะยงชิดเป็นผลไม้ที่.........<br>(แก้ไขข้อมูลและเพิ่มรูป ไปที่ เนื้อหา -> หน้า -> มะยงชิด)" } },
                                     { attr: { title: "กล้วยหอมทอง", content: "กล้วยหอมทองเป็นผลไม้ที่.........<br>(แก้ไขข้อมูลและเพิ่มรูป ไปที่ เนื้อหา -> หน้า -> กล้วยหอมทอง)" } } ] } }
                ]

  def initialize(site)
    @site = site
  end

  def build_dummy_content
    dummy_contact_info
    dummy_content
  end

  private

  def dummy_content
    page_position = 1
    DUMMY_MENUS.each_with_index do |m, i|
      m = m[:menu]
      menu = Menu.new m[:attr]
      menu.site = @site
      menu.position = i + 1

      if m[:type] == "page"
        page = Page.new m[:page].first[:attr]
        page.site = @site
        page.position = page_position
        page_position = page_position + 1
        puts page.valid?
        page.save

        menu.menuable = page
        menu.save
      elsif m[:type] == "category"
        category = Category.new m[:category][:attr]
        category.site = @site

        m[:pages].each do |p|
          page = Page.new p[:attr]
          page.site = @site
          page.position = page_position
          page_position = page_position + 1
          page.save
          category.pages << page
        end
        category.save
        menu.menuable = category
        menu.save
      end
    end
  end

  def dummy_contact_info
    contact_info = ContactInfo.new DUMMY_CONTACT_INFO
    contact_info.site = @site
    contact_info.save
  end

end
