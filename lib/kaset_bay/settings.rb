WEBSITE_NAME = "Kaset Bay"
META_DESCRIPTION = "A free website builder to create an SEO friendly and responsive website within minutes."
META_KEYWORDS = "free website builder, no coding, seo friendly, responsive"
DOMAIN_HOST = "kasetbay.com"
ADMIN_EMAIL = "kasetbay@gmail.com"
FACEBOOK_URL = "https://facebook.com/kasetbay"
SMTP = { address: "smtp.mandrillapp.com",
         port: 587,
         enable_starttls_auto: true,
         user_name: ENV['SMTP_USER_NAME'],
         password: ENV['SMTP_PASSWORD'],
         authentication: "login",
         domain: DOMAIN_HOST
       }
ACCEPTED_IMAGE_TYPES = ["image/jpeg", "image/jpg", "image/pjpeg", "image/png", "image/x-png"]
CATEGORY_LIMIT = 20
PAGE_LIMIT = 30
PICTURE_LIMIT = 60
DEFAULT_COUNTRY = "ประเทศไทย"
