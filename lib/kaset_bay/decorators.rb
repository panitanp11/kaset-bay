ActionView::Helpers::FormBuilder.class_eval do
  def label_with_hint(method, text = nil, options = {}, &block)
    hint_translation = I18n.translate(method, default: "", scope: [object.class.model_name.plural.underscore, "hint"])
    if hint_translation.present?
      hint_translation = " - <span class='s'>#{hint_translation}</span>"
    end

    "#{label(method, text, options, &block)}#{hint_translation}".html_safe
  end
end
